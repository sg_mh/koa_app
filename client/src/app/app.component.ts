import { Component, OnInit, ViewChild } from '@angular/core';
import { SocketService } from './socket.service';
import { EVENTS } from '../../../common/contract';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [SocketService]
})
export class AppComponent {
  public roomId: string = location.pathname;
  public tabInfoArray: any[];
  public userCount: number;

  @ViewChild('appTabs') appTabs;

  constructor(public socketService: SocketService) { }

  // addTabInfo
  addTabInfo() {
    this.socketService.addTabInfo(this.roomId);
  }

  // todo
  convert2TabInfo(tabInfo: any, active: boolean, removable: boolean) {
    return { tabId: tabInfo.Id, name: tabInfo.Name, active: active, removable: removable }
  }

  // remove
  removeTabHandler(tabInfo) {
    this.tabInfoArray = this.tabInfoArray.filter(val => val.tabId !== tabInfo.tabId);
    this.socketService.removeTabInfo(this.roomId, tabInfo.tabId);
  }

  // init
  ngOnInit() {
    this.tabInfoArray = new Array();
    // event recive
    this.socketService.roomEventRecieve(this.roomId).subscribe((data: any) => {
      switch (data.action) {
        case EVENTS.ROOM_INIT:
          let initFlg = true;
          data.payload.forEach(val => {
            this.tabInfoArray.push(
              this.convert2TabInfo(val, initFlg, !initFlg)
            );
            initFlg = false;
          });
          break;
        case EVENTS.JOINED_MENBERS:
          this.userCount = data.payload;
          break;
        case EVENTS.ADD_TABINFO:
          this.tabInfoArray.push(this.convert2TabInfo(data.payload, false, true));
          break;
        case EVENTS.REMOVE_TABINFO:
          // todo tabcomponent
          this.tabInfoArray = this.tabInfoArray.filter(val => val.tabId !== data.payload);
          this.appTabs.onRemoveTabInfo(data.payload);
          break;
      }
    });
  }
}
