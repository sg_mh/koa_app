import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AceEditorComponent } from './ng2-ace/ng2-ace-editor';
import { AppComponent } from './app.component';

//socket.io service
import { SocketService } from "./socket.service";
import { TabComponent } from './tab/tab.component';
import { TabDirective } from './tab/tab.directive';
import { EditorComponent } from './editor/editor.component';

@NgModule({
  declarations: [
    AppComponent,
    AceEditorComponent,
    TabComponent,
    EditorComponent,
    TabDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    SocketService //inject
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
