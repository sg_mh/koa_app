export let diff = (oldValue: string, newValue: string) => {
    let commonStart = 0;
    while (commonStart < newValue.length &&
    newValue.charAt(commonStart) == oldValue.charAt(commonStart)) {
        commonStart++;
    }
    let commonEnd = 0;
    while (commonEnd < (newValue.length - commonStart) &&
    commonEnd < (oldValue.length - commonStart) &&
    newValue.charAt(newValue.length - commonEnd - 1) ==
    oldValue.charAt(oldValue.length - commonEnd - 1)) {
        commonEnd++;
    }
    let removed = oldValue.substr(commonStart, oldValue.length - commonStart - commonEnd);
    let inserted = newValue.substr(commonStart, newValue.length - commonStart - commonEnd);
    if (! (removed.length || inserted)) {
        return null;
    }
    return  {
        start: commonStart,
        removeEnd: removed.length,
        insertValue: inserted
    }
};


export let apply = (
    text: string, 
    patch:{
        start: number,
        removeEnd: number,
        insertValue: string}) => {
            return text.substr(0, patch.start) 
            + patch.insertValue + text.substr(patch.start + patch.removeEnd);
}