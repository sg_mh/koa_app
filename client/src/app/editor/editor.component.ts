import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SocketService } from '../socket.service';

import { diff, apply } from '../../../../common/diff_patch';
import { DEFAULT_CODE, LANGUAGE_LIST, THEME_LIST } from './editor.constants';

import { EVENTS } from '../../../../common/contract';

import 'brace';
declare let ace: any;
let Range = ace.acequire('ace/range').Range;


@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css'],
  providers: [SocketService]
})
export class EditorComponent implements OnInit {
  @Input() tabId: string;
  @Input() roomId: string;
  sequence: number = 0;
  //roomid: string = location.pathname;

  text: string = DEFAULT_CODE;
  oldValue: string = DEFAULT_CODE;

  mode: string = 'html';
  theme: string = 'monokai';

  cursorPointer = {};
  selectionPointer = {};

  languageList: any = LANGUAGE_LIST;
  themeList: any = THEME_LIST;
  @ViewChild('editor') editor;

  options: any = {
    maxLines: 1000,
    printMargin: false,
    displayIndentGuides: true,
    highlightActiveLine: true,
    cursorStart: 1,
    behavioursEnabled: true
  };

  constructor(
    public socketService: SocketService
  ) { }

  ngOnInit() {
    console.log(this.tabId);
  }


  //init view
  ngAfterViewInit() {


    this.editor.getEditor().getSelection().on('changeCursor', () => {
      if (this.editor.isSilent()) {
        return;
      }
      let cursor = this.editor.getEditor().getCursorPosition();
      cursor.eventSrc = 'changeCuror';
      cursor.SelectionRange = this.editor.getEditor().getSelectionRange();
      this.socketService.sendCursor(cursor, this.roomId, this.tabId);
    });

    //connect socket.io server
    this.socketService.receivePatch(this.roomId, this.tabId).subscribe((data: any) => {
      console.log(`tabId: ${this.tabId}`);
      //subscribe patch data
      console.log(++this.sequence, data.action, data.payload, this.roomId + '/' + this.tabId);
      switch (data.action) {

        //patch action
        case EVENTS.CODE:
          if (this.execJudgment(data.payload.tabId)) {
            let newValue = apply(this.text, data.payload.patch);
            this.text = newValue;
            this.oldValue = newValue;
          }
          break;

        //language change action
        case EVENTS.LANGUAGE:
          if (this.execJudgment(data.payload.tabId)) {
            this.mode = data.payload.value;
          }
          break;

        //move cursor action
        case EVENTS.CURSOR:
          if (this.execJudgment(data.payload.tabId)) {
            this.reciveChangeCursor(data.payload.cursor);
          }
          break;

        //leave user action
        case EVENTS.LEAVE:
          this.removeMarkar(data.payload);
          break;

        //init code action
        case EVENTS.INIT_CODE:
          if (this.execJudgment(data.payload.tabId)) {
            this.text = data.payload.code;
            this.oldValue = data.payload.code;
          }
          break;

        //sync cursor
        case EVENTS.SYNC_CURSOR:
          if (this.execJudgment(data.payload.tabId)) {
            let syncCursor = data.payload.syncCursor;
            this.editor.getEditor().moveCursorToPosition(syncCursor);
            syncCursor.eventSrc = 'syncCursor';
            this.socketService.sendCursor(syncCursor, this.roomId, this.tabId);
          }
          break;
        //init cursor
        case EVENTS.INIT_CURSOR:
          if (this.execJudgment(data.payload.tabId)) {
            this.reciveChangeCursor(data.payload.cursor);
          }
          break;
      }
    });
  }

  // cursorEvent
  private reciveChangeCursor(cursorInfo: any) {
    let cursor = cursorInfo;
    let socketID = cursor.socketID;
    this.removeMarkar(socketID);
    let range = new Range(cursor.row, cursor.column, cursor.row, cursor.column + 1);
    this.cursorPointer[cursor.socketID] = this.editor.getEditor().getSession().addMarker(range, "marked", "text", true);

    if (cursor.SelectionRange) {
      if (this.selectionPointer[socketID]) {
        this.editor.getEditor().getSession().removeMarker(this.selectionPointer[socketID]);
      }
      if (cursor.SelectionRange.start.row !== cursor.SelectionRange.end.row || cursor.SelectionRange.start.column !== cursor.SelectionRange.end.column) {
        let selection = new Range(cursor.SelectionRange.start.row, cursor.SelectionRange.start.column, cursor.SelectionRange.end.row, cursor.SelectionRange.end.column);
        this.selectionPointer[socketID] = this.editor.getEditor().getSession().addMarker(selection, "ace_selection", "text", false);
      }
    }
  }

  //editor on change
  onChange(newValue) {
    let patch = diff(this.oldValue, newValue);
    this.oldValue = newValue;
    if (patch) {
      console.log('patch', patch);
      let cursor = this.editor.getEditor().getCursorPosition();
      this.socketService.sendPatch(patch, this.roomId, this.tabId);
    }
  }

  changeLanguage(language){
    
  }

  //remove marker
  private removeMarkar(socketID: string) {
    if (this.cursorPointer[socketID]) {
      this.editor.getEditor().getSession().removeMarker(this.cursorPointer[socketID]);
      return true;
    }
    return false;
  }

  private handleCursorEvent(cursor) {

  }



  private execJudgment(tabId: String): boolean {
    if (this.tabId === tabId) {
      return true;
    } else {
      return false;
    }
  }

}
