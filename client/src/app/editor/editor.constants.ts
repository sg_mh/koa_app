//import theme
import 'brace';
import 'brace/theme/ambiance';
import 'brace/theme/chaos';
import 'brace/theme/chrome';
import 'brace/theme/clouds';
import 'brace/theme/clouds_midnight';
import 'brace/theme/cobalt';
import 'brace/theme/crimson_editor';
import 'brace/theme/dawn';
import 'brace/theme/dreamweaver';
import 'brace/theme/eclipse';
import 'brace/theme/github';
import 'brace/theme/idle_fingers';
import 'brace/theme/iplastic';
import 'brace/theme/katzenmilch';
import 'brace/theme/kr_theme';
import 'brace/theme/kuroir';
import 'brace/theme/merbivore';
import 'brace/theme/merbivore_soft';
import 'brace/theme/mono_industrial';
import 'brace/theme/monokai';
import 'brace/theme/pastel_on_dark';
import 'brace/theme/solarized_dark';
import 'brace/theme/solarized_light';
import 'brace/theme/sqlserver';
import 'brace/theme/terminal';
import 'brace/theme/textmate';
import 'brace/theme/tomorrow';
import 'brace/theme/tomorrow_night';
import 'brace/theme/tomorrow_night_blue';
import 'brace/theme/tomorrow_night_bright';
import 'brace/theme/tomorrow_night_eighties';
import 'brace/theme/twilight';
import 'brace/theme/vibrant_ink';
import 'brace/theme/xcode';


//import language mode
import 'brace/mode/javascript';
import 'brace/mode/jsx';
import 'brace/mode/json';
import 'brace/mode/css';
import 'brace/mode/java';
import 'brace/mode/scala';
import 'brace/mode/php';
import 'brace/mode/properties';
import 'brace/mode/python';
import 'brace/mode/ruby';
import 'brace/mode/perl';
import 'brace/mode/golang';
import 'brace/mode/swift';
import 'brace/mode/c_cpp';
import 'brace/mode/jsp';
import 'brace/mode/sql';
import 'brace/mode/html';
import 'brace/mode/xml';


//export language list
export const LANGUAGE_LIST = [
    {mode: 'javascript', text: 'javascript'},
    {mode: 'jsx', text: 'jsx'},
    {mode: 'json', text: 'json'},
    {mode: 'css', text: 'css'},
    {mode: 'java', text: 'java'},
    {mode: 'scala', text: 'scala'},
    {mode: 'php', text: 'php'},
    {mode: 'properties', text: 'properties'},
    {mode: 'python', text: 'python'},
    {mode: 'ruby', text: 'ruby'},
    {mode: 'perl', text: 'perl'},
    {mode: 'golang', text: 'golang'},
    {mode: 'swift', text: 'swift'},
    {mode: 'c_cpp', text: 'c++'},
    {mode: 'jsp', text: 'jsp'},
    {mode: 'sql', text: 'sql'},
    {mode: 'html', text: 'html'},
    {mode: 'xml', text: 'xml'}
];

//export theme list
export const THEME_LIST = [
    {theme: 'ambiance'},
    {theme: 'chaos'},
    {theme: 'chrome'},
    {theme: 'clouds'},
    {theme: 'clouds_midnight'},
    {theme: 'cobalt'},
    {theme: 'crimson_editor'},
    {theme: 'dawn'},
    {theme: 'dreamweaver'},
    {theme: 'eclipse'},
    {theme: 'github'},
    {theme: 'idle_fingers'},
    {theme: 'iplastic'},
    {theme: 'katzenmilch'},
    {theme: 'kr_theme'},
    {theme: 'kuroir'},
    {theme: 'merbivore'},
    {theme: 'merbivore_soft'},
    {theme: 'mono_industrial'},
    {theme: 'monokai'},
    {theme: 'pastel_on_dark'},
    {theme: 'solarized_dark'},
    {theme: 'solarized_light'},
    {theme: 'sqlserver'},
    {theme: 'terminal'},
    {theme: 'textmate'},
    {theme: 'tomorrow'},
    {theme: 'tomorrow_night'},
    {theme: 'tomorrow_night_blue'},
    {theme: 'tomorrow_night_bright'},
    {theme: 'tomorrow_night_eighties'},
    {theme: 'twilight'},
    {theme: 'vibrant_ink'},
    {theme: 'xcode'}
];

//default code
export const DEFAULT_CODE = "";
// export const DEFAULT_CODE =`<html>
//   <head>
//     <title>let's coding!</title>
//   </head>
//   <body>
//     <p>...</p>
//   </body>
// </html>`

