import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import * as io from "socket.io-client";
import { environment } from '../environments/environment';
import { EVENTS } from '../../../common/contract';


@Injectable()
export class SocketService {

    private static socket: any;
    private host: string = environment.host;

    constructor() {
        console.log(`production=${environment.production}`);
    }

    public roomEventRecieve(roomId: string): Observable<any> {
        let socketUrl = this.host;
        console.log(`connect to ${socketUrl}`);
        SocketService.socket = io.connect(socketUrl);
        SocketService.socket.on(EVENTS.CONNECT, () => this.connect(roomId));
        SocketService.socket.on(EVENTS.DISCONNECT, () => this.disconnect());
        SocketService.socket.on(EVENTS.ERROR, (error: string) => {
            console.log(`ERROR: "${error}" (${socketUrl})`);
        });
        return Observable.create((observer: any) => {
            SocketService.socket.on(EVENTS.JOINED_MENBERS, (userInfo: any) => observer.next({ action: EVENTS.JOINED_MENBERS, payload: userInfo }));
            SocketService.socket.on(EVENTS.ROOM_INIT, (tabInfo: any) => observer.next({ action: EVENTS.ROOM_INIT, payload: tabInfo }));
            SocketService.socket.on(EVENTS.ADD_TABINFO, (tabInfo: any) => observer.next({ action: EVENTS.ADD_TABINFO, payload: tabInfo }));
            SocketService.socket.on(EVENTS.REMOVE_TABINFO, (tabId: any) => observer.next({ action: EVENTS.REMOVE_TABINFO, payload: tabId }));
            return () => SocketService.socket.close();
        });
    }

    // https://github.com/jussikinnula/angular2-socketio-chat-example/blob/master/src/app/shared/socket.service.ts
    public receivePatch(roomId: string, tabId: string): Observable<any> {
        // Join
        console.log(`join To roomId: ${roomId} tabId: ${tabId}`)
        SocketService.socket.emit(EVENTS.JOIN_TAB, roomId, tabId);
        return Observable.create((observer: any) => {
            //receive patch 
            SocketService.socket.on(EVENTS.CODE, (patch: any, id: string) => observer.next({ action: EVENTS.CODE, payload: { patch: patch, tabId: id } }));
            SocketService.socket.on(EVENTS.LANGUAGE, (value: any, id: string) => observer.next({ action: EVENTS.LANGUAGE, payload: { value: value, tabId: id } }));
            SocketService.socket.on(EVENTS.CURSOR, (cursor: any, id: string) => observer.next({ action: EVENTS.CURSOR, payload: { cursor: cursor, tabId: id } }));
            SocketService.socket.on(EVENTS.LEAVE, (socketID: any) => observer.next({ action: EVENTS.LEAVE, payload: socketID }));
            SocketService.socket.on(EVENTS.INIT_CODE, (code: any, id: string) => observer.next({ action: EVENTS.INIT_CODE, payload: { code: code, tabId: id } }));
            SocketService.socket.on(EVENTS.SYNC_CURSOR, (syncCursor: any, id: string) => observer.next({ action: EVENTS.SYNC_CURSOR, payload: { syncCursor: syncCursor, tabId: id } }));
            SocketService.socket.on(EVENTS.INIT_CURSOR, (cursor: any, id: string) => observer.next({ action: EVENTS.INIT_CURSOR, payload: { cursor: cursor, tabId: id } }));
            return () => SocketService.socket.close();
        });
    }

    //send patch to server
    public sendPatch(patch: string, roomId: string, tabId: string) {
        SocketService.socket.emit(EVENTS.CODE, patch, roomId, tabId);
    }

    //change language event
    public sendLanguage(value: string, roomId: string, tabId: string) {
        SocketService.socket.emit(EVENTS.LANGUAGE, value, roomId, tabId);
    }

    //seding cursor
    public sendCursor(cursor: any, roomId, tabId) {
        SocketService.socket.emit(EVENTS.CURSOR, cursor, roomId, tabId);
    }

    // addTabInfo
    public addTabInfo(roomId: any) {
        SocketService.socket.emit(EVENTS.ADD_TABINFO, roomId);
    }

    // remove tab
    public removeTabInfo(roomId, tabId) {
        SocketService.socket.emit(EVENTS.REMOVE_TABINFO, roomId, tabId);
    }

    //on connect
    private connect(roomid: string) {
        //join room
        console.log(`join room ${roomid}`);
        SocketService.socket.emit(EVENTS.JOIN_ROOM, roomid);
    }

    //on disconnect
    private disconnect() {
        console.log('disconnect');
    }
}