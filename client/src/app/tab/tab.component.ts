import { Component, Input, Output, EventEmitter, ContentChildren, QueryList, HostBinding } from '@angular/core';
import { TabDirective as AppTab } from './tab.directive';

@Component({
  selector: 'app-tabs',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent {
  public tabInfoArray: Array<AppTab> = [];
  constructor() { };

  public addTabInfo(tabInfo: AppTab) {
    this.tabInfoArray.push(tabInfo);
    tabInfo.active = this.tabInfoArray.length === 1 && tabInfo.active !== false;
  }

  public removeTabInfo(tabInfo: AppTab) {
    this.onRemoveTabInfo(tabInfo);
    tabInfo.removed.emit(tabInfo);
  }

  public onRemoveTabInfo(tabInfo: AppTab) {
    let tabIndex = this.tabInfoArray.findIndex(val => val.tabId === tabInfo.tabId);
    this.tabInfoArray = this.tabInfoArray.filter(val => val.tabId !== tabInfo.tabId);
    if (tabInfo.active) {
      this.tabInfoArray[tabIndex - 1].active = true;
    }
  }
};
