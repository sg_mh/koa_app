import { Directive, OnDestroy, EventEmitter, Input, Output, HostBinding } from '@angular/core';
import { TabComponent as AppTabs } from './tab.component';

@Directive({ selector: 'app-tab, [app-tab]' })
export class TabDirective implements OnDestroy {
    private _active: boolean;
    @Input() public removable: boolean;
    @Input() public tabId: string;
    @Input() public name: string;

    @HostBinding('class.active')
    @Input() public get active() { return this._active; }
    public set active(active) {
        if (!active) {
            this._active = active;
            this.deselect.emit(this);
            return;
        }
        this._active = active;
        this.select.emit(this);
        this.appTabs.tabInfoArray.forEach((tabInfo: TabDirective) => {
            if (tabInfo !== this) {
                tabInfo.active = false;
            }
        });
    };

    @Output() public select: EventEmitter<TabDirective> = new EventEmitter();
    @Output() public deselect: EventEmitter<TabDirective> = new EventEmitter();
    @Output() public removed: EventEmitter<TabDirective> = new EventEmitter();

    @HostBinding('class.tab-pane') addClass = true;

    constructor(public appTabs: AppTabs) {
        this.appTabs.addTabInfo(this);
    };
    
    ngOnInit() { };
    ngOnDestroy() { };
};