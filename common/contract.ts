//socket io event contract
export const EVENTS = {
    CONNECT: 'connect',
    DISCONNECT: 'disconnect',
    ERROR: 'error',
    CODE:'code',
    LANGUAGE: 'language',
    CURSOR: 'cursor',
    LEAVE: 'leaveUser',
    JOIN_ROOM: 'join room',
    INIT_CODE: 'initCode',
    JOINED_MENBERS:'joinedMenbers',
    SYNC_CURSOR:'syncCursor',
    ADD_TABINFO:'addTabInfo',
    ROOM_INIT:"roomInitalaize",
    JOIN_TAB:"join tab",
    REMOVE_TABINFO:"removeTabInfo",
    INIT_CURSOR:"initCursor",
};