"use strict";
exports.diff = function (oldValue, newValue) {
    var commonStart = 0;
    while (commonStart < newValue.length &&
        newValue.charAt(commonStart) == oldValue.charAt(commonStart)) {
        commonStart++;
    }
    var commonEnd = 0;
    while (commonEnd < (newValue.length - commonStart) &&
        commonEnd < (oldValue.length - commonStart) &&
        newValue.charAt(newValue.length - commonEnd - 1) ==
            oldValue.charAt(oldValue.length - commonEnd - 1)) {
        commonEnd++;
    }
    var removed = oldValue.substr(commonStart, oldValue.length - commonStart - commonEnd);
    var inserted = newValue.substr(commonStart, newValue.length - commonStart - commonEnd);
    if (!(removed.length || inserted)) {
        return null;
    }
    return {
        start: commonStart,
        removeEnd: removed.length,
        insertValue: inserted
    };
};
exports.apply = function (text, patch) {
    return text.substr(0, patch.start)
        + patch.insertValue + text.substr(patch.start + patch.removeEnd);
};
