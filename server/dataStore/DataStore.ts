import * as RoomUtils from '../utils/RoomInfoUtils';
export interface DataStore {
    // Init
    initDataStore(): void;
    // Get@RoomInfo
    getRoomInfo(roomId: string): RoomUtils.RoomInfo;
    // Get@UserInfo
    getUserInfo(roomId: string, userId: string): RoomUtils.UserInfo;
    // Get@TabInfo
    getTabInfo(roomId: string, tabId: string): RoomUtils.TabInfo;
    
    // Add@RoomInfo
    addRoomInfo(roomInfo: RoomUtils.RoomInfo): void;
    // Add@UserInfo
    addUserInfo(userInfo: RoomUtils.UserInfo, roomId: string): void;
    // Add@TabInfo
    addTabInfo(tabInfo: RoomUtils.TabInfo, roomId: string): void;
    // Add@CursorInfo
    addCursorInfo(cursorInfo: RoomUtils.CursorInfo, roomId: string, tabId: string): void;

    // Remove@RoomInfo
    rmvRoomInfo(roomId: string): void;
    // Remove@UserInfo
    rmvUserInfo(roomId: string, userId: string): void;
    // Remove@TabInfo
    rmvTabInfo(roomId: string, tabId: string): void;
    // Remove@CursorInfo
    rmvCursorInfo(roomId: string, tabId: string, socketId: string): void;

    // Update@TabInfo.Name
    updTabName(roomId: string, tabId: string, name: string): void;
    // Update@TabInfo.CodeName
    updCodeName(roomId: string, tabId: string, codeName: string): void;
    // Update@TabInfo.CodeValue
    updCodeValue(roomId: string, tabId: string, codeValue: string): void;
    // Update@TabInfo.CursorInfo
    updCursorInfo(roomId: string, tabId: string, socketId: string, row: number, column: number): void;
}