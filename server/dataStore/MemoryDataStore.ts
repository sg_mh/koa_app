import { DataStore } from './DataStore';
import * as RoomUtils from '../utils/RoomInfoUtils';
import * as Lodash from "lodash";

export class MemoryDataStore implements DataStore {
    // RoomInfo
    _roomInfo: RoomUtils.RoomInfo[];
    constructor() { this.initDataStore(); }

    // Init
    initDataStore = (): void => { this._roomInfo = new Array<RoomUtils.RoomInfo>() }
    // Get@RoomInfo
    getRoomInfo = (roomId: string): RoomUtils.RoomInfo => this.getInfo(this._roomInfo, roomId);
    // Get@UserInfo
    getUserInfo = (roomId: string, userId: string): RoomUtils.UserInfo => this.getInfo(this.getRoomInfo(roomId).Users, userId);
    // Get@TabInfo
    getTabInfo = (roomId: string, tabId: string): RoomUtils.TabInfo => this.getInfo(this.getRoomInfo(roomId).Tabs, tabId);
    // Get@CursorInfo(SocketId)
    getCursorInfo = (roomId: string, tabId: string, socketId: string): RoomUtils.CursorInfo => Lodash.find(this.getTabInfo(roomId, tabId).CursorInfo, v => v.SocketId === socketId);

    // Add@RoomInfo
    addRoomInfo = (roomInfo: RoomUtils.RoomInfo): void => { this.addInfo(this._roomInfo, roomInfo); }
    // Add@UserInfo
    addUserInfo = (userInfo: RoomUtils.UserInfo, roomId: string): void => { this.addInfo(this.getRoomInfo(roomId).Users, userInfo); }
    // Add@TabInfo
    addTabInfo = (tabInfo: RoomUtils.TabInfo, roomId: string): void => { this.addInfo(this.getRoomInfo(roomId).Tabs, tabInfo); }
    // Add@CursorInfo
    addCursorInfo = (cursorInfo: RoomUtils.CursorInfo, roomId: string, tabId: string): void => { this.addInfo(this.getTabInfo(roomId, tabId).CursorInfo, cursorInfo); }

    // Remove@RoomInfo
    rmvRoomInfo = (roomId: string): void => { this.rmvInfo(this._roomInfo, roomId); }
    // Remove@UserInfo
    rmvUserInfo = (roomId: string, userId: string): void => { this.rmvInfo(this.getRoomInfo(roomId).Users, userId); }
    // Remove@TabInfo
    rmvTabInfo = (roomId: string, tabId: string): void => { this.rmvInfo(this.getRoomInfo(roomId).Tabs, tabId); }
    // Remove@CursorInfo
    rmvCursorInfo = (roomId: string, tabId: string, socketId: string): void => {
        Lodash.remove(this.getTabInfo(roomId, tabId).CursorInfo, v => v.SocketId === socketId);
    }

    // Update@TabInfo.Name
    updTabName = (roomId: string, tabId: string, name: string): void => { this.getTabInfo(roomId, tabId).Name = name; }
    // Update@TabInfo.CodeName
    updCodeName = (roomId: string, tabId: string, codeName: string): void => { this.getTabInfo(roomId, tabId).CodeName = codeName; }
    // Update@TabInfo.CodeValue
    updCodeValue = (roomId: string, tabId: string, codeValue: string): void => { this.getTabInfo(roomId, tabId).CodeValue = codeValue; }
    // Update@TabInfo.CursorInfo
    updCursorInfo = (roomId: string, tabId: string, socketId: string, row: number, column: number): void => {
        let cursorInfoArray: RoomUtils.CursorInfo[] = this.getTabInfo(roomId, tabId).CursorInfo;
        let cursorInfo = Lodash.find(cursorInfoArray, v => v.SocketId === socketId);
        cursorInfo.BeforeRow = cursorInfo.Row;
        cursorInfo.BeforeColumn = cursorInfo.Column;
        cursorInfo.Row = row;
        cursorInfo.Column = column;
    };

    // Common@Get
    getInfo = (infoArray: any[], key: string): any => Lodash.find(infoArray, v => v.Id === key);
    // Common@Add
    addInfo = (infoArray: any[], info: any): void => { infoArray.push(info); }
    // Common@Remove
    rmvInfo = (infoArray: any[], key: string): void => { Lodash.remove(infoArray, v => v.Id === key); }

}