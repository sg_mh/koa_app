import * as Router from 'koa-router';
import * as fs from 'fs';
import * as Utils from '../utils/ServerUtils'
/** Router */
export function KoaRouter(): Router {
    const _router: any = new Router();
    /** Redirect */
    _router.get("/codist", (ctx, next) => {
        ctx.redirect("/codist/" + Utils.getUniversallyId())
    });
    /** /:RoomId */
    _router.get("/codist/:roomid", (ctx, next) => {
        let indexContent: any = false;
        try {
            indexContent = fs.readFileSync(Utils.Config.IndexPath, { encoding: Utils.Config.Encoding });
        } catch (err) {
            console.error(err);
        }
        ctx.body = indexContent;
        next;
    });
    return _router;
};