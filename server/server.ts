import * as Koa from 'koa';
import * as Serve from 'koa-static';
import * as Socket from 'socket.io';
import * as Logger from 'koa-logger';
import * as Utils from './utils/ServerUtils'
import * as RoomInfoUtils from './utils/RoomInfoUtils'
import * as Patch from '../common/diff_patch';
//import * as _Patch from 'diff-match-patch';
import { KoaRouter } from './router/Router';
import { EVENTS } from '../common/contract';
import { MemoryDataStore } from './dataStore/MemoryDataStore';
import * as Lodash from "lodash";

let dataStore: any = new MemoryDataStore();
const app: any = new Koa();
app.use(Logger());
app.use(Serve(Utils.Config.StaticPath));
app.use(KoaRouter().routes());
app.use(KoaRouter().allowedMethods());

const server: any = app.listen(Utils.Config.Port);
const io: any = Socket.listen(server);

// Socket Connection
io.on(EVENTS.CONNECT, (socket) => {

    // join room
    socket.on(EVENTS.JOIN_ROOM, (roomId: string) => {
        socket.join(roomId);
        socket.handshake.roomId = roomId;
        if (dataStore.getRoomInfo(roomId) == null) {
            let roomInfo = RoomInfoUtils.createRoomInfo(roomId, socket.id);
            dataStore.addRoomInfo(roomInfo);
            socket.handshake.userId = roomInfo.Users[0].Id;
        } else {
            let userInfo = addUserCursorInfo(roomId, socket.id);
            socket.handshake.userId = userInfo.Id;
        }
        console.log(`roomId:${socket.handshake.roomId}`);
        console.log(`userId:${socket.handshake.userId} @socketId:${socket.id}`);
        // emit:JOINED_MENBERS
        joinMenbersEmit(io, roomId);
        // emit:ROOM_INIT
        io.sockets.connected[socket.id].emit(EVENTS.ROOM_INIT, dataStore.getRoomInfo(roomId).Tabs);
    });

    // join tab
    socket.on(EVENTS.JOIN_TAB, (roomId: string, tabId: string) => {
        socket.join(Utils.getEmitId(roomId, tabId));
        // emit:INIT_CODE
        io.sockets.connected[socket.id].emit(EVENTS.INIT_CODE, dataStore.getTabInfo(roomId, tabId).CodeValue, tabId);
        // emit:CURSOR
        socket.to(Utils.getEmitId(roomId, tabId)).broadcast.emit(EVENTS.CURSOR, { row: 0, column: 0, socketID: socket.id });
        Lodash.forEach(dataStore.getTabInfo(roomId, tabId).CursorInfo, v => {
            if (socket.id !== v.SocketId) {
                // emit:INIT_CURSOR
                io.sockets.connected[socket.id].emit(EVENTS.INIT_CURSOR, { row: v.Row, column: v.Column, socketID: v.SocketId }, tabId);
            }
        });
    });

    // code
    socket.on(EVENTS.CODE, (code: any, roomId: string, tabId: string) => {
        dataStore.updCodeValue(roomId, tabId, Patch.apply(dataStore.getTabInfo(roomId, tabId).CodeValue, code));
        // emit:CODE
        socket.to(Utils.getEmitId(roomId, tabId)).broadcast.emit(EVENTS.CODE, code, tabId);
        // emit:SYNC_CURSOR
        syncCursor(io, socket, roomId, tabId);
    });

    // language
    socket.on(EVENTS.LANGUAGE, (language: string, roomId: string, tabId: string) => {
        // emit:LANGUAGE
        socket.to(Utils.getEmitId(roomId, tabId)).emit(EVENTS.LANGUAGE, language, tabId)
    });

    // cursor
    socket.on(EVENTS.CURSOR, (points: any, roomId: string, tabId: string) => {
        points.socketID = socket.id;
        dataStore.updCursorInfo(roomId, tabId, socket.id, points.row, points.column);
        // emit:CURSOR
        socket.to(Utils.getEmitId(roomId, tabId)).broadcast.emit(EVENTS.CURSOR, points, tabId);
    });

    // disconnect
    socket.on(EVENTS.DISCONNECT, () => {
        let roomId = socket.handshake.roomId;
        let userId = socket.handshake.userId;
        if (userId) {
            dataStore.rmvUserInfo(roomId, userId);
        }
        // emit:JOINED_MENBERS
        joinMenbersEmit(io, roomId);
        // emit:LEAVE
        Lodash.forEach(dataStore.getRoomInfo(roomId).Tabs, v => {
            dataStore.rmvCursorInfo(roomId, v.Id, socket.id);
            io.to(Utils.getEmitId(roomId, v.Id)).emit(EVENTS.LEAVE, socket.id);
        });

    });

    // add tab
    socket.on(EVENTS.ADD_TABINFO, (roomId: string) => {
        let newTabInfo = RoomInfoUtils.getNewTabInfo();
        dataStore.addTabInfo(newTabInfo, roomId);
        Lodash.forEach(dataStore.getRoomInfo(roomId).Users, v => {
            dataStore.addCursorInfo(RoomInfoUtils.getNewCursorInfo(v.SocketId), roomId, newTabInfo.Id);
        });
        // emit:ADD_TABINFO
        io.to(roomId).emit(EVENTS.ADD_TABINFO, newTabInfo);
    });

    // remove tab
    socket.on(EVENTS.REMOVE_TABINFO, (roomId: string, tabId: string) => {
        dataStore.rmvTabInfo(roomId, tabId);
        // emit:REMOVE_TABINFO
        socket.to(roomId).broadcast.emit(EVENTS.REMOVE_TABINFO, tabId);
    });
});

// join menber emit
let joinMenbersEmit = (io: any, roomId: string): void => {
    let userInfo = dataStore.getRoomInfo(roomId).Users;
    // emit:JOINED_MENBERS
    io.to(roomId).emit(EVENTS.JOINED_MENBERS, userInfo.length);
};

// add RoomInfo.Users & TabInfo.CursorInfo
let addUserCursorInfo = (roomId: string, socketId: string): RoomInfoUtils.UserInfo => {
    let userInfo = RoomInfoUtils.getNewUserInfo(socketId);
    dataStore.addUserInfo(userInfo, roomId);
    Lodash.forEach(dataStore.getRoomInfo(roomId).Tabs, v => {
        dataStore.addCursorInfo(RoomInfoUtils.getNewCursorInfo(socketId), roomId, v.Id);
    });
    return userInfo;
};

// SyncCursor
let syncCursor = (io: any, socket: any, roomId: string, tabId: string): void => {
    let userId = socket.handshake.userId;
    let cursorInfoArray: RoomInfoUtils.CursorInfo[] = dataStore.getTabInfo(roomId, tabId).CursorInfo;
    let selfCursorInfo: RoomInfoUtils.CursorInfo = dataStore.getCursorInfo(roomId, tabId, socket.id);
    let syncCursorInfo: RoomInfoUtils.CursorInfo[] = new Array();

    let x = selfCursorInfo.Column - selfCursorInfo.BeforeColumn;
    let y = selfCursorInfo.Row - selfCursorInfo.BeforeRow;
    Lodash.find(cursorInfoArray, v => {
        if (socket.id != v.SocketId) {
            if (selfCursorInfo.BeforeRow == v.Row && selfCursorInfo.BeforeColumn <= v.Column) {
                v.Column = v.Column + x;
                if (y != 0) {
                    v.Row = selfCursorInfo.Row;
                }
                syncCursorInfo.push(v);
            } else if (selfCursorInfo.BeforeRow < v.Row) {
                if (y != 0) {
                    v.Row = v.Row + y;
                    syncCursorInfo.push(v);
                }
            }
        }
    });
    // Sync
    Lodash.forEach(syncCursorInfo, v => {
        // Update@SyncUser.CursorInfo
        dataStore.updCursorInfo(roomId, tabId, v.SocketId, v.Row, v.Column);
        // emit:SYNC_CURSOR
        io.sockets.connected[v.SocketId].emit(EVENTS.SYNC_CURSOR, { row: v.Row, column: v.Column }, tabId);
    });
}