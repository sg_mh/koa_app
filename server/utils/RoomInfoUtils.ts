import * as Utils from "./ServerUtils";

// RoomInfo
export class RoomInfo {
    Id: string;
    Tabs: TabInfo[];
    Users: UserInfo[];
};

// UserInfo
export class UserInfo {
    Id: string;
    SocketId: string;
};

// TabInfo
export class TabInfo {
    Id: string;
    Name: string;
    CodeId: string;
    CodeName: string;
    CodeValue: string;
    CursorInfo: CursorInfo[];
};

// CursorInfo
export class CursorInfo {
    Id: string;
    SocketId: string;
    Row: number;
    Column: number;
    BeforeRow: number;
    BeforeColumn: number;
};

// NewCursorInfo
export function getNewCursorInfo(socketId: string) {
    let cursorInfo = new CursorInfo();
    cursorInfo.Id = Utils.getUniversallyId();
    cursorInfo.SocketId = socketId;
    cursorInfo.Row = 0;
    cursorInfo.Column = 0;
    cursorInfo.BeforeRow = 0;
    cursorInfo.BeforeColumn = 0;
    return cursorInfo;
}

// NewUserInfo
export function getNewUserInfo(socketId: string): UserInfo {
    let userInfo = new UserInfo();
    userInfo.Id = Utils.getUniversallyId();
    userInfo.SocketId = socketId;
    return userInfo;
};

// NewTabInfo
export function getNewTabInfo(): TabInfo {
    let tabInfo = new TabInfo();
    tabInfo.Id = Utils.getUniversallyId();
    tabInfo.Name = Utils.Config.DefaultTab;
    tabInfo.CodeId = Utils.getUniversallyId();
    tabInfo.CodeName = Utils.Config.DefaultCodeName;
    tabInfo.CodeValue = Utils.Config.DefaultCode;
    tabInfo.CursorInfo = new Array<CursorInfo>();
    return tabInfo;
};

// NewRoomInfo
export function getNewRoomInfo(roomId: string): RoomInfo {
    let roomInfo = new RoomInfo();
    roomInfo.Id = roomId;
    roomInfo.Tabs = new Array<TabInfo>();
    roomInfo.Users = new Array<UserInfo>();
    return roomInfo;
};

// createRoomInfo
export function createRoomInfo(roomId: string, socketId: string): RoomInfo {
    let roomInfo = this.getNewRoomInfo(roomId);
    let cursorInfo = this.getNewCursorInfo(socketId);
    roomInfo.Tabs.push(this.getNewTabInfo(socketId));
    roomInfo.Users.push(this.getNewUserInfo(socketId));
    roomInfo.Tabs[0].CursorInfo.push(cursorInfo);
    return roomInfo;
};