import * as Uuid from "node-uuid";
import * as Path from "path";
import * as Lodash from "lodash";

// UniversallyId
export function getUniversallyId(): string {
    return Uuid.v4();
}
// Config
export let Config = {
    IndexPath: Path.resolve(__dirname, "../../client/dist/index.html"),
    Encoding: "utf8",
    StaticPath: Path.resolve(__dirname, "../../client/dist"),
    Port: process.env.PORT || 3000,
    DefaultCode: "<html>\n    <head>\n        <title>let's coding!</title>\n    </head>\n    <body>\n        <p>...</p>\n    </body>\n</html>",
    DefaultTab: "Untitled",
    DefaultCodeName: "Untitled",
    DefaultUserName: "John Doe.."
}
// EmitId
export function getEmitId(roomId: string, tabId: string): string {
    return roomId + "/" + tabId;
}